import nodeResolve from '@rollup/plugin-node-resolve';

export default {
  input: 'dist/esm/index.js',
  output: {
    file: 'dist/plugin.js',
    format: 'iife',
    name: 'capacitorPlugin', // TODO: change this
    globals: {
      '@capacitor/core': 'capacitorExports',
      'code-push': 'codePush',
      '@capacitor/filesystem': 'capacitorFilesystem',
      '@capacitor-community/http': 'capacitorCHttp',
      '@capacitor/device': 'capacitorDevice',
      '@capacitor/dialog': 'capacitorDialog',
      'code-push/script/acquisition-sdk':'codePushAcquisitionSDK'
    },
    sourcemap: true,
  },
  plugins: [
    nodeResolve({
      // allowlist of dependencies to bundle in
      // @see https://github.com/rollup/plugins/tree/master/packages/node-resolve#resolveonly
      resolveOnly: ['lodash'],
    }),
  ],
};
